#ifndef TESTTHING_H
#define TESTTHING_H

#include <QObject>
#include <QQmlEngine>

class TestThing : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON
public:
    explicit TestThing(QObject *parent = nullptr);

    Q_INVOKABLE void foo(const QString &bla);
    Q_INVOKABLE void foo(TestThing *a);
};

#endif // TESTTHING_H
