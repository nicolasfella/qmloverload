import QtQuick
import QtQuick.Window

import qmloverload

Item {
    property QtObject obj: TestThing;
    Component.onCompleted: TestThing.foo(obj)
}
