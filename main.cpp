#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "testthing.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    TestThing thing;
    qmlRegisterSingletonInstance<TestThing>("qmloverload", 1, 0, "TestThing", &thing);

    QQmlApplicationEngine engine;
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed,
        &app, []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.loadFromModule("qmloverload", "Main");

    return app.exec();
}
